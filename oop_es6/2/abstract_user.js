class AbstractUser {

    constructor(user) {
        if (new.target === AbstractUser) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }

        if (this.getPermissionString === undefined && typeof(this.getPermissionString) !== 'function') {
            throw new TypeError("Must override method");
        }

        if (this.validate(user)) {
            Object.assign(this, user);
        }

        if (!user.hasOwnProperty("enabled")) {
            this.enabled = false;
        }
    }

    throwPropertyError(property) {
        throw "property " + property + " is required";
    }

    throwGenericError(errorMsg) {
        throw errorMsg;
    }

    checkProperties(user, ...properties) {
        properties.forEach(property => {
            if (!user.hasOwnProperty(property)) {
                this.throwPropertyError(property);
            }
        });
    }

    validate(user) {
        this.checkProperties(user, "username", "password", "name", "surname");

        if (!this.isValidUsername(user.username)) {
            this.throwGenericError("username is not valid: it should be an email");
        }

        if (!this.isValidPassword(user.password)) {
            this.throwGenericError("password is not valid: minimum eight characters, at least one letter and one number");
        }

        return true;
    }

    isValidUsername(username) {
        return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
            username
        );
    }

    isValidPassword(password) {
        return /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(
            password
        );
    }

    //override
    toString() {
        return "User with username " + this.username + " is " + ((this.enabled) ? "" : "not ") + "enabled";
    }

    write() {
        if (!this.getPermission().contains("W")) {
            throw new "not permitted";
        } else {
            console.log("Writing...");
        }
    }

    read() {
        console.log("Reading...");
    }
}