class CoffeeDispenser {

    constructor(coffee1Quantity, coffee2Quantity, coffee3Quantity) {
        this.coffee1Quantity = coffee1Quantity;
        this.coffee2Quantity = coffee2Quantity;
        this.coffee3Quantity = coffee3Quantity;
    }

    get totalQuantity() {
        return this.coffee1Quantity + this.coffee2Quantity + this.coffee3Quantity;
    }

    set add1(quantity) {
        this["coffee1Quantity"] += quantity;
    }

    set add2(quantity) {
        this["coffee2Quantity"] += quantity;
    }

    set add3(quantity) {
        this["coffee3Quantity"] += quantity;
    }

}