let getTodos = () => {

    let url = "https://jsonplaceholder.typicode.com/todos";

    return fetch(url).then(function(response) {
        return response.json();
    }).catch(error => console.log('error', error));
}

let getUserById = (userId) => {

    let url = "https://jsonplaceholder.typicode.com/users/" + userId;

    return fetch(url).then(function(response) {
        return response.json();
    }).catch(error => console.log('error', error));
}